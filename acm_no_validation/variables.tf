variable "domains" {
  description = "Domains to include on the certificate (the first one will be your main domain)"
  type        = "list"
}
