data "aws_vpc" "vpc" {
  id = "${var.vpc_id}"
}

resource "aws_security_group" "docker_sg" {
  name   = "${var.identifier}-docker"
  vpc_id = "${var.vpc_id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${data.aws_vpc.vpc.cidr_block}"]
  }

  ingress {
    from_port   = 5672
    to_port     = 5672
    protocol    = "tcp"
    cidr_blocks = ["${data.aws_vpc.vpc.cidr_block}"]
  }

  ingress {
    from_port   = 15672
    to_port     = 15672
    protocol    = "tcp"
    cidr_blocks = ["${data.aws_vpc.vpc.cidr_block}"]
  }

  ingress {
    from_port   = 9200
    to_port     = 9200
    protocol    = "tcp"
    cidr_blocks = ["${data.aws_vpc.vpc.cidr_block}"]
  }

  ingress {
    from_port   = 2376
    to_port     = 2376
    protocol    = "tcp"
    cidr_blocks = ["${data.aws_vpc.vpc.cidr_block}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Application     = "${var.identifier}"
    Confidentiality = "StrictlyConfidential"
    Creator         = "marcelomarra"
    Environment     = "${var.identifier}"
    Name            = "${var.identifier}-docker"
    Owner           = "DevSquad"
    Product         = "${var.identifier}"
  }
}

resource "aws_security_group" "vpn_sg" {
  name   = "${var.identifier}-vpn"
  vpc_id = "${var.vpc_id}"

  ingress {
    from_port   = 1139
    to_port     = 1139
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 1194
    to_port     = 1194
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Application     = "${var.identifier}"
    Confidentiality = "StrictlyConfidential"
    Creator         = "marcelomarra"
    Environment     = "${var.identifier}"
    Name            = "${var.identifier}-vpn"
    Owner           = "DevSquad"
    Product         = "${var.identifier}"
  }
}

resource "aws_security_group" "windows_sg" {
  name   = "${var.identifier}-windows"
  vpc_id = "${var.vpc_id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${data.aws_vpc.vpc.cidr_block}"]
  }

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["${data.aws_vpc.vpc.cidr_block}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Application     = "${var.identifier}"
    Confidentiality = "StrictlyConfidential"
    Creator         = "marcelomarra"
    Environment     = "${var.identifier}"
    Name            = "${var.identifier}-windows"
    Owner           = "DevSquad"
    Product         = "${var.identifier}"
  }
}

resource "aws_security_group" "helix_sg" {
  name   = "${var.identifier}-helix"
  vpc_id = "${var.vpc_id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${data.aws_vpc.vpc.cidr_block}"]
  }

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["${data.aws_vpc.vpc.cidr_block}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Application     = "${var.identifier}"
    Confidentiality = "StrictlyConfidential"
    Creator         = "marcelomarra"
    Environment     = "${var.identifier}"
    Name            = "${var.identifier}-helix"
    Owner           = "DevSquad"
    Product         = "${var.identifier}"
  }
}

resource "aws_key_pair" "key_pair" {
  key_name   = "${var.identifier}"
  public_key = "${var.ssh_public_key}"
}

data "aws_availability_zones" "allzones" {}

resource "aws_instance" "docker" {
  ami                         = "${var.ami}"
  instance_type               = "${var.docker_instance_type}"
  subnet_id                   = "${var.private_subnet_ids[0]}"
  user_data                   = "${var.user_data}"
  associate_public_ip_address = false
  key_name                    = "${aws_key_pair.key_pair.key_name}"
  vpc_security_group_ids      = ["${aws_security_group.docker_sg.id}"]

  credit_specification {
    cpu_credits = "unlimited"
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Application     = "${var.identifier}"
    Confidentiality = "StrictlyConfidential"
    Creator         = "marcelomarra"
    Environment     = "${var.identifier}"
    Name            = "${var.identifier}-docker"
    Owner           = "DevSquad"
    Product         = "${var.identifier}"
  }

  root_block_device {
    volume_size = "${var.volume_size}"
    volume_type = "standard"
  }
}

resource "aws_instance" "vpn" {
  ami                         = "${var.vpn_ami}"
  instance_type               = "${var.vpn_instance_type}"
  subnet_id                   = "${var.public_subnet_ids[0]}"
  user_data                   = "${var.vpn_user_data}"
  associate_public_ip_address = false
  key_name                    = "${aws_key_pair.key_pair.key_name}"
  vpc_security_group_ids      = ["${aws_security_group.vpn_sg.id}"]
  associate_public_ip_address = true

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Application     = "${var.identifier}"
    Confidentiality = "StrictlyConfidential"
    Creator         = "marcelomarra"
    Environment     = "${var.identifier}"
    Name            = "${var.identifier}-vpn"
    Owner           = "DevSquad"
    Product         = "${var.identifier}"
  }

  root_block_device {
    volume_size = "${var.vpn_volume_size}"
    volume_type = "standard"
  }
}

resource "aws_instance" "windows" {
  ami                         = "${var.windows_ami}"
  instance_type               = "${var.windows_instance_type}"
  subnet_id                   = "${var.private_subnet_ids[0]}"
  user_data                   = "${var.windows_user_data}"
  associate_public_ip_address = false
  key_name                    = "${aws_key_pair.key_pair.key_name}"
  vpc_security_group_ids      = ["${aws_security_group.windows_sg.id}"]

  credit_specification {
    cpu_credits = "unlimited"
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Application     = "${var.identifier}"
    Confidentiality = "StrictlyConfidential"
    Creator         = "marcelomarra"
    Environment     = "${var.identifier}"
    Name            = "${var.identifier}-windows"
    Owner           = "DevSquad"
    Product         = "${var.identifier}"
  }

  root_block_device {
    volume_size = "${var.volume_size}"
    volume_type = "standard"
  }
}

resource "aws_instance" "helix" {
  ami                         = "${var.helix_ami}"
  instance_type               = "${var.helix_instance_type}"
  subnet_id                   = "${var.private_subnet_ids[0]}"
  user_data                   = "${var.helix_user_data}"
  associate_public_ip_address = false
  key_name                    = "${aws_key_pair.key_pair.key_name}"
  vpc_security_group_ids      = ["${aws_security_group.helix_sg.id}"]

  credit_specification {
    cpu_credits = "unlimited"
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Application     = "${var.identifier}"
    Confidentiality = "StrictlyConfidential"
    Creator         = "marcelomarra"
    Environment     = "${var.identifier}"
    Name            = "${var.identifier}-helix"
    Owner           = "DevSquad"
    Product         = "${var.identifier}"
  }
}

resource "aws_route53_record" "vpn" {
  zone_id = "${var.zone_id}"
  name    = "vpn.${var.main_domain}"
  type    = "A"
  ttl     = "1"
  records = ["${aws_instance.vpn.public_ip}"]
}

resource "aws_route53_record" "vpn_internal" {
  zone_id = "${var.zone_id}"
  name    = "vpn-internal.${var.main_domain}"
  type    = "A"
  ttl     = "1"
  records = ["${aws_instance.vpn.private_ip}"]
}

resource "aws_lb" "nlb" {
  name               = "${var.identifier}-nlb"
  load_balancer_type = "network"

  subnet_mapping {
    subnet_id     = "${var.public_subnet_ids[0]}"
    allocation_id = "${var.elb_public_ip_ids[0]}"
  }

  subnet_mapping {
    subnet_id     = "${var.public_subnet_ids[1]}"
    allocation_id = "${var.elb_public_ip_ids[1]}"
  }

  tags = {
    Application     = "${var.identifier}"
    Confidentiality = "StrictlyConfidential"
    Creator         = "marcelomarra"
    Environment     = "${var.identifier}"
    Name            = "${var.identifier}"
    Owner           = "DevSquad"
    Product         = "${var.identifier}"
  }
}

resource "aws_lb_target_group" "https" {
  name              = "${var.identifier}-https"
  port              = "80"
  protocol          = "TCP"
  vpc_id            = "${var.vpc_id}"
  target_type       = "instance"
  proxy_protocol_v2 = true

  stickiness {
    type    = "lb_cookie"
    enabled = false
  }

  health_check {
    interval = "30"
    protocol = "TCP"
    port     = "22"
  }
}

resource "aws_lb_target_group" "http" {
  name        = "${var.identifier}-http"
  port        = "8080"
  protocol    = "TCP"
  vpc_id      = "${var.vpc_id}"
  target_type = "instance"

  stickiness {
    type    = "lb_cookie"
    enabled = false
  }

  health_check {
    interval = "30"
    protocol = "TCP"
    port     = "22"
  }
}

resource "aws_lb_target_group_attachment" "https" {
  target_group_arn = "${aws_lb_target_group.https.arn}"
  target_id        = "${aws_instance.docker.id}"
  port             = "80"
}

resource "aws_lb_target_group_attachment" "http" {
  target_group_arn = "${aws_lb_target_group.http.arn}"
  target_id        = "${aws_instance.docker.id}"
  port             = "8080"
}

resource "aws_lb_listener" "https" {
  load_balancer_arn = "${aws_lb.nlb.arn}"
  port              = "443"
  protocol          = "TLS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "${var.acm_certificate_arn}"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.https.arn}"
  }
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = "${aws_lb.nlb.arn}"
  port              = "80"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.http.arn}"
  }
}
