output "nlb_dns_name" {
  value = "${aws_lb.nlb.dns_name}"
}

output "nlb_zone_id" {
  value = "${aws_lb.nlb.zone_id}"
}

output "this_instance_id" {
  value = "${aws_instance.docker.id}"
}

output "windows_instance_private_ip" {
  value = "${aws_instance.windows.private_ip}"
}

output "docker_instance_private_ip" {
  value = "${aws_instance.docker.private_ip}"
}

output "helix_instance_private_ip" {
  value = "${aws_instance.helix.private_ip}"
}
