resource "aws_route53_health_check" "app" {
  fqdn              = "${local.fqdn}"
  port              = 443
  type              = "HTTPS"
  resource_path     = "${var.health_check_path}"
  failure_threshold = "5"
  request_interval  = "30"
  measure_latency   = true

  tags = {
    Application     = "${var.identifier}"
    Confidentiality = "StrictlyConfidential"
    Creator         = "marcelomarra"
    Environment     = "${var.identifier}"
    Name            = "${local.fqdn}"
    Owner           = "DevSquad"
    Product         = "${var.main_domain}"
  }
}

resource "aws_cloudwatch_metric_alarm" "alarm" {
  actions_enabled           = "true"
  alarm_actions             = ["${var.alarm_sns_topic_arn}"]
  ok_actions                = ["${var.alarm_sns_topic_arn}"]
  insufficient_data_actions = ["${var.alarm_sns_topic_arn}"]
  alarm_description         = "${local.fqdn}"
  alarm_name                = "${local.fqdn}"
  comparison_operator       = "LessThanThreshold"

  dimensions = {
    "HealthCheckId" = "${aws_route53_health_check.app.id}"
  }

  evaluation_periods = "1"
  metric_name        = "HealthCheckStatus"
  namespace          = "AWS/Route53"
  period             = "60"
  statistic          = "Minimum"
  threshold          = "1"
  treat_missing_data = "breaching"
  unit               = ""
}
