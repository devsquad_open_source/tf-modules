variable "identifier" {
  description = "Backend identifier"
}

variable "kms_arn" {
  description = "KMS key arn"
}
