variable "zone_id" {
  description = "Route53 zone id"
}

variable "main_domain" {
  description = "Domain to act as the certificate domain name"
}

variable "subdomains" {
  description = "Subdomains to add as alternative names"
  type        = "list"

  default = [
    "*",
  ]
}
