variable "networks" {
  description = "Network list to attach nginx container"
  type        = "list"
}

variable "nginx_extra_settings" {
  default = ""
}

variable "tld" {
  description = "TLD served by this nginx instance"
  default     = "$domain"
}

variable "extra_locations" {
  default = ""
}

variable "main_location_pre" {
  default = ""
}

variable "main_location_post" {
  default = ""
}

variable "nginx_image" {}

variable "read_timeout" {
  default = "15s"
}

variable "connect_timeout" {
  default = "5s"
}

variable "proxy_real_cidr" {
  default = "10.0.0.0/8"
}
