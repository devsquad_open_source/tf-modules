resource "aws_security_group" "openvpn" {
  name = "${var.identifier}-openvpn"

  ingress {
    from_port   = "${var.vpn_port}"
    to_port     = "${var.vpn_port}"
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "vpn" {
  instance_type = "t2.micro"
  ami           = "ami-633ba70c"

  vpc_security_group_ids = [
    "${aws_security_group.vpn.id}",
  ]

  lifecycle {
    create_before_destroy = true
  }

  key_name = "${var.key_pair_name}"

  connection {
    user  = "${var.ssh_remote_user}"
    agent = true
  }

  provisioner "remote-exec" {
    inline = [
      "docker volume create --name ${var.vpn_data}",
      "docker run -v ${var.vpn_data}:/etc/openvpn --rm kylemanna/openvpn ovpn_genconfig -u udp://${aws_instance.vpn.public_dns}",
      "yes 'yes' | docker run -v ${var.vpn_data}:/etc/openvpn --rm -i kylemanna/openvpn ovpn_initpki nopass",
      "docker run -v ${var.vpn_data}:/etc/openvpn -d -p ${var.vpn_port}:${var.vpn_port}/udp --cap-add=NET_ADMIN kylemanna/openvpn",
      "docker run -v ${var.vpn_data}:/etc/openvpn --rm -it kylemanna/openvpn easyrsa build-client-full ${var.vpn_client_name} nopass",
      "docker run -v ${var.vpn_data}:/etc/openvpn --rm kylemanna/openvpn ovpn_getclient ${var.vpn_client_name} > ~/${var.vpn_client_name}.ovpn",
    ]
  }

  provisioner "local-exec" {
    command = "ssh-keyscan -T 120 ${aws_instance.vpn.public_ip} >> ~/.ssh/known_hosts"
  }

  provisioner "local-exec" {
    command = "scp ${var.ssh_remote_user}@${aws_instance.vpn.public_ip}:~/${var.vpn_client_name}.ovpn ."
  }

  tags = {
    Application     = "${var.identifier}"
    Confidentiality = "StrictlyConfidential"
    Creator         = "marcelomarra"
    Environment     = "${var.identifier}"
    Name            = "${var.identifier}-openvpn"
    Owner           = "DevSquad"
    Product         = "${var.identifier}"
  }
}
