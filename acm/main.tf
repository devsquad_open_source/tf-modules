locals {
  main_domain         = "${var.domains[0]}"
  alternative_domains = "${distinct(concat(slice(var.domains, 1, length(var.domains)), formatlist("*.%s", var.domains)))}"
}

data "aws_route53_zone" "zone" {
  count        = "${length(var.domains)}"
  name         = "${element(var.domains,count.index)}."
  private_zone = false
}

resource "aws_acm_certificate" "cert" {
  domain_name               = "${local.main_domain}"
  validation_method         = "DNS"
  subject_alternative_names = "${local.alternative_domains}"
}

resource "aws_route53_record" "cert_validation" {
  count = "${length(local.alternative_domains)+1}"

  #   count   = "${length(aws_acm_certificate.cert.domain_validation_options)}"
  name    = "${lookup(aws_acm_certificate.cert.domain_validation_options[count.index], "resource_record_name")}"
  type    = "${lookup(aws_acm_certificate.cert.domain_validation_options[count.index], "resource_record_type")}"
  zone_id = "${element(data.aws_route53_zone.zone.*.id, index(var.domains, "${replace(lookup(aws_acm_certificate.cert.domain_validation_options[count.index], "domain_name"), "*.", "")}"))}"
  records = ["${lookup(aws_acm_certificate.cert.domain_validation_options[count.index], "resource_record_value")}"]
  ttl     = 1
}

resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = "${aws_acm_certificate.cert.arn}"
  validation_record_fqdns = ["${aws_route53_record.cert_validation.*.fqdn}"]
}
