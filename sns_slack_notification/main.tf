resource "aws_sns_topic" "this" {
  name = "${var.sns_topic_name}"
}

resource "aws_sns_topic_subscription" "sns_notify_slack" {
  topic_arn = "${aws_sns_topic.this.arn}"
  protocol  = "lambda"
  endpoint  = "${aws_lambda_function.notify_slack.arn}"
}

resource "aws_lambda_permission" "sns_notify_slack" {
  statement_id  = "AllowExecutionFromSNS"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.notify_slack.function_name}"
  principal     = "sns.amazonaws.com"
  source_arn    = "${aws_sns_topic.this.arn}"
}

data "null_data_source" "lambda_file" {
  inputs {
    filename = "${substr("${path.module}/functions/notify_slack.py", length(path.cwd) + 1, -1)}"
  }
}

resource "random_id" "random" {
  keepers {
    timestamp = "${aws_sns_topic.this.arn}"
  }

  byte_length = 8
}

data "null_data_source" "lambda_archive" {
  inputs {
    filename = "${substr("${path.module}/functions/notify_slack-${random_id.random.dec}.zip", length(path.cwd) + 1, -1)}"
  }
}

data "archive_file" "notify_slack" {
  type        = "zip"
  source_file = "${data.null_data_source.lambda_file.outputs["filename"]}"
  output_path = "${data.null_data_source.lambda_archive.outputs["filename"]}"
}

resource "aws_lambda_function" "notify_slack" {
  filename = "${data.archive_file.notify_slack.0.output_path}"

  function_name = "${var.lambda_function_name}"

  role             = "${aws_iam_role.lambda.arn}"
  handler          = "notify_slack.lambda_handler"
  source_code_hash = "${data.archive_file.notify_slack.0.output_base64sha256}"
  runtime          = "python3.6"
  timeout          = 30
  kms_key_arn      = "${var.kms_key_arn}"

  environment {
    variables = {
      SLACK_WEBHOOK_URL = "${var.slack_webhook_url}"
      SLACK_CHANNEL     = "${var.slack_channel}"
      SLACK_USERNAME    = "${var.slack_username}"
      SLACK_EMOJI       = "${var.slack_emoji}"
    }
  }

  lifecycle {
    ignore_changes = [
      "filename",
      "last_modified",
    ]
  }
}
