variable "allowed_cidrs" {
  type    = "list"
  default = []
}

variable "identifier" {}

variable "engine_version" {
  default = "11.00.6594.0.v1"
}

variable "username" {}

variable "database" {
  description = "Database to restore the latest bkp"
}

variable "account_id" {}

variable "volume_size" {}

variable "vpc_id" {
  description = "VPC ID"
}

variable "subnet_ids" {
  description = "VPC private subnet ID"
  type        = "list"
}

variable "instance_id" {}

variable "backup_bucket" {}

variable "kms_key_id" {}

variable "deletion_protection" {}

variable "instance_type" {
  default = "db.t2.medium"
}

variable "maintenance_window" {
  default = "Wed:21:01-Wed:23:59"
}

variable "major_engine_version" {
  default = "11.00"
}

variable "enabled_cloudwatch_logs_exports" {
  description = "List of log types to enable for exporting to CloudWatch logs. If omitted, no logs will be exported. Valid values (depending on engine): alert, audit, error, general, listener, slowquery, trace, postgresql (PostgreSQL), upgrade (PostgreSQL)."
  default     = []
}
