variable "aws_access_key_id" {
  description = "AWS Credential"
}

variable "aws_secret_access_key" {
  description = "AWS Credential"
}

variable "aws_region" {
  description = "AWS Region"
}

variable "vault_backend_s3_bucket" {
  description = "Vault S3 Backend"
}
